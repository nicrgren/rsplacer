
use std::env;
use std::path::PathBuf;
use std::process::Command;

use failure::Error;


// Transform command is a command provided by the user.
// It is run on each string matched by regex.
#[derive(PartialEq, Debug)]
pub struct ExternalExe {
    command_path: PathBuf,
}


impl ExternalExe {
    pub fn new<S: Into<String>>(command: S) -> Result<ExternalExe, Error> {
        // priorotize current dir to find the executeable
        let mut wd: PathBuf = env::current_dir()?;
        let command = command.into();
        wd.push(&command);

        if wd.exists() {
            return Ok(ExternalExe {
                command_path: wd,
            });
        }

        let path_var = match env::vars().find(|&(ref key, _)| key == "PATH") {
            Some((_, val)) => val,
            None => {
                return Err(format_err!("Could not exec \"{}\". File not found",
                                       &command))
            }
        };

        for s in path_var.split(':') {
            let mut p = PathBuf::from(s);
            p.push(&command);
            if p.exists() {
                return Ok(ExternalExe {
                    command_path: p,
                })
            }
        }

        Err(format_err!("Could not exec \"{}\". File not found", &command))
    }

    pub fn run_with_arg(&self, arg: &str) -> Result<Vec<u8>, Error> {
        let output = Command::new(&self.command_path)
            .arg(arg)
            .output()?;

        if !output.status.success() {
            return Err(format_err!("Command \"{:?} {}\"failed with output:\n{}", 
                                    &self.command_path,
                                    arg,
                                    String::from_utf8_lossy(&output.stderr)))
        }

        let mut res = output.stdout;
        res.pop();
        Ok(res)
    }
}

