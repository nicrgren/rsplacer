
use std::fs::File;
use std::convert::AsRef;
use std::io;
use Error;

pub enum Input {
    FromFile(File),
    FromStdin,
}


impl Input {
    pub fn from_file<T: AsRef<str>>(path: T) -> Result<Input, Error> {
        let file = File::open(path.as_ref())?;
        Ok(Input::FromFile(file))
    }


    pub fn from_stdin() -> Input {
        Input::FromStdin
    }
}


impl io::Read for Input {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        match self {

            &mut Input::FromFile(ref mut file) => {
                file.read(buf)
            },

            &mut Input::FromStdin => {
                io::stdin().read(buf)
            }
        }
    }
}
