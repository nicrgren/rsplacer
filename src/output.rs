
use std::fs;
use std::io;

use ::Error;

pub enum Output {
    ToFile(fs::File),
    Stdout,
}


impl Output {
    pub fn to_stdout() -> Output {
        Output::Stdout
    }

    pub fn to_file<T: AsRef<str>>(file_path: T)
                                  -> Result<Output, Error> {
        let file = fs::File::open(file_path.as_ref())?;

        Ok(Output::ToFile(file))
    }
}


impl io::Write for Output {

    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {

        match self {
            &mut Output::ToFile(ref mut file) => {
                file.write(buf)
            }

            &mut Output::Stdout => {
                io::stdout().write(buf)
            },
        }
    }

    fn flush(&mut self) -> io::Result<()> {
        match self {
            &mut Output::ToFile(ref mut file) => file.flush(),
            &mut Output::Stdout => io::stdout().flush(),

        }
    }
}
