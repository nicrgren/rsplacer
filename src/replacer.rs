
use regex;

use std::io::{self, Read, Write};

use input::Input;
use output::Output;
use transform_command::ExternalExe;

use std::str;
use ::Error;

#[derive(Debug, Fail)]
pub enum ReplacerError {
    #[fail(display = "Invalid regex \"{}\": {}", given, regex_err)]
    InvalidRegex {
        given: String,
        regex_err: regex::Error,
    },

    #[fail(display = "Invalid argument, shuld be on the form: <[e, s]>/<match>/<replace>/. But was: \"{}\"", given_str)]
    InvalidStringArg {
        given_str: String,
    },


    #[fail(display = "Failed to find external command \"{}\"", script_str)]
    ExternalExeNotFound {
        script_str: String,
    },

    #[fail(display = "Error executing external executeable with arg \"{}\"", argument)]
    ExternalExeFailed {
        argument: String,
    },



    #[fail(display = "Unknown command \"{}\"", given_command)]
    UnknownCommand {
        given_command: String,
    }
}

pub struct Replacer<'a> {
    regex: regex::Regex,
    method: ReplaceMethod<'a>,
}


#[derive(PartialEq, Debug)]
pub enum ReplaceMethod<'a> {
    Simple(&'a str), // replace all matches with this str.
    ExternalExe(ExternalExe),
}


impl<'a> Replacer<'a> {
    pub fn from_arg_string(s: &'a str) -> Result<Replacer, ReplacerError> {
        use self::ReplacerError::*;

        match ArgParser(s).collect::<Vec<&'a str>>().as_slice() {

            // Only availble command right now.
            &[ "s", regex_str, replace_str ] => {
                let regex = regex::Regex::new(regex_str)
                    .map_err(|e| InvalidRegex {
                        given: s.into(),
                        regex_err: e,
                    })?;

                Ok(Replacer {
                    regex: regex,
                    method: ReplaceMethod::Simple(replace_str),
                })
            }


            &[ "e", regex_str, script_str ] => {
                let regex = regex::Regex::new(regex_str)
                    .map_err(|e| InvalidRegex {
                        given: s.into(),
                        regex_err: e,
                    })?;

                let external_exe = ExternalExe::new(script_str)
                    .map_err(|_| ReplacerError::ExternalExeNotFound {
                        script_str: script_str.into(),
                    })?;


                Ok(Replacer {
                    regex: regex,
                    method: ReplaceMethod::ExternalExe(external_exe),
                })

            }


            &[ command, _, _ ]=> {
                Err(ReplacerError::UnknownCommand { given_command: command.into() })
            }



            _ =>
                Err(ReplacerError::InvalidStringArg {
                    given_str: s.into(),
                }),
        }
    }

    pub fn replace(&mut self,
                   mut input: Input,
                   output: Output) -> Result<(), Error>
    {
        // Quick n' dirty
        // TODO(niro): Optimize this....
        // TODO(niro): Handle errors better...


        let mut buf_out = io::BufWriter::new(output);

        match self.method {
            ReplaceMethod::ExternalExe(ref mut exe) => {

                let mut line_buf = String::new();
                input.read_to_string(&mut line_buf)?;
                let bs = line_buf.as_bytes();
                let mut i = 0;

                for m in self.regex.find_iter(&line_buf) {
                    buf_out.write_all(&bs[i..m.start()])?;
                    // write the replacement.
                    let replacement = exe.run_with_arg(m.as_str())
                        .map_err(|_| ReplacerError::ExternalExeFailed {
                            argument: m.as_str().into(),
                        })?;

                    buf_out.write_all(&replacement)?;
                    i = m.end();
                }

                // write last part.
                buf_out.write_all(&bs[i..])?;
                buf_out.flush()?;
                Ok(())
            },

            ReplaceMethod::Simple(s) => {

                let mut line_buf = String::new();
                match input.read_to_string(&mut line_buf) {
                    Ok(_) => {
                        let replaced = self.regex.replace_all(&line_buf, s);
                        // TODO(niro): Handle potential error...
                        buf_out.write(replaced.as_bytes())?;
                        buf_out.flush()?;
                        Ok(())
                    },

                    Err(e) => Err(format_err!("Error when reading: {}", e)),
                }
            },
        }

    }
}


struct ArgParser<'a>(&'a str);

impl<'a> Iterator for ArgParser<'a> {
    type Item = &'a str;

    fn next(&mut self) -> Option<&'a str> {

        let mut skip = false;
        for (i, c) in self.0.chars().enumerate() {
            match c {
                _ if skip => skip = false,
                '\\' => skip = true,
                '/' => {
                    let res = &self.0[0..i];
                    self.0 = &self.0[i+1..];
                    return Some(res)
                }

                _ => (),
            }
        }

        None

    }
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn test_parse_simple_str() {
        let mut p = ArgParser("s/hejsan/dejsan/");
        assert_eq!(Some("s"), p.next());
        assert_eq!(Some("hejsan"), p.next());
        assert_eq!(Some("dejsan"), p.next());
        assert_eq!(None, p.next());
    }

    fn test_replacer_from_arg_str() {
        let rep = Replacer::from_arg_string("s/tomat/soppa/");
        assert!(rep.is_ok());

        let rep = rep.unwrap();
        assert_eq!(rep.method, ReplaceMethod::Simple("soppa"));
        assert_eq!(rep.regex.as_str(), "tomat");

    }
}




