

#![feature(slice_patterns)]

extern crate regex;
extern crate structopt;

#[macro_use] extern crate structopt_derive;
#[macro_use] extern crate failure;
#[macro_use] extern crate failure_derive;

pub use failure::Error;
use structopt::StructOpt;
use std::io;
use std::io::Write;

// Mods
mod replacer;
mod transform_command;
mod output;
mod input;



#[derive(StructOpt, Debug)]
#[structopt(name = "rsplacer", about = "This is a very useful usage string")]
struct Opt {

    #[structopt(help = r"Command string.
For simple replace it must be on the form 's/<regex to match>/<str to replace matches>/'.
For replace using external script: 'e/<str to match>/<external exe>/'")]
    command: String,

    #[structopt(help = "File to search, if empty, stdin is used")]
    file: Option<String>,

    // #[structopt(short = "e",
    //             long = "command",
    //             help = "External script to execute for each match, result of this script is used as replace string")]
    // external_script: Option<String>,


    // #[structopt(short = "i",
    //             long = "inplace",
    //             help = "Edit file inplace",
    //             default_value = "false")]
    // edit_inplace: bool,

}


fn main() {
    let opt = Opt::from_args();

    let mut replacer = match replacer::Replacer::from_arg_string(&opt.command) {
        Ok(r) => r,
        Err(e) => {
            println!("{}", e);
            return;
        }
    };


    let input = match opt.file {

        Some(file_str) => {
            match input::Input::from_file(&file_str) {
                Ok(in_src) => in_src,
                Err(e) => {
                    println!("Could not open {}: {}", file_str, e);
                    return;
                },
            }
        },


        None => {
            input::Input::from_stdin()
        },

    };


    let output = output::Output::to_stdout();

    match replacer.replace(input, output) {
        Ok(_) => (),

        Err(e) => {
            write!(io::stderr(), "Error during replace: {}", e).unwrap();
            ()
        }
    };


    // let regex = match Regex::new("(-*[0-9]*)(px)") {
    //     Err(err) => {
    //         println!("Invalid regex: {}", err);
    //         return;
    //     }
    //     Ok(r) => r,
    // };

    // let s = read_file(&test_file).unwrap();
    // let command = TransformCommand::new(command_str).unwrap();
    // let mut out = std::io::stdout();
    // let captures = regex.captures_iter(&s);

    // let mut i = 0;
    // for _cap in captures {
    //     if let Some(cap) = _cap.get(0) {
    //         // Write everything from start of string, to our current match.
    //         out.write_all(s[i..cap.start()].as_bytes()).unwrap();

    //         // Run the command and print the output
    //         let new_bs = match command.run_with_arg(cap.as_str()) {

    //             Err(e) => {
    //                 println!("{}", e.cause());
    //                 return;
    //             }
    //             Ok(bs) => bs,
    //         };

    //         out.write_all(&new_bs).unwrap();
    //         i = cap.end();
    //     }
    // }
}


// fn read_file(filename: &str) -> Result<String, Error> {
//     let mut f = File::open(filename)?;
//     let mut buffer = String::new();

//     f.read_to_string(&mut buffer)?;

//     return Ok(buffer);
// }
